## Description
Service Dev Dashbord Siterdidik

### Team

| Name                                                                   | Role                 |
| :--------------------------------------------------------------------- | :------------------- |
| Ahmad Zulfi                                                            | Analisis, Programmer |
| Yhoga                                                            | Analisis |
| Lukman                                                            | Analisis |
| Aby                                                            | Analisis |
| Ahmad Rivaiy                                                            | Programmer |

## Application Information

### Engine

| Name     | Version | References                              |
| :------- | :------ | :-------------------------------------- |
| ExpressJS  | v4.17.3   | https://expressjs.com/                    |
| NodeJS      | v16.20.0     | https://nodejs.org/download/release/v16.20.2/node-v16.20.2-x64.msi |
| Sequelize | v6   | https://sequelize.org/docs/v6/core-concepts/model-querying-basics/                |
| PostgreSQL | v13   | https://www.postgresql.org/download/                |

## Getting Started
1. Copy file `.env.example` to `.env` and please fill this field with your data

    ```.dotenv
    PORT=
    ```
2. Install require Package
    ```
    npm install
    ```
    ```
    npm install --save-dev sequelize-cli
    ```
2. Create `Database name, but same with your data in .env file `
2. Run Migrating
    ```
    npx sequelize-cli db:migrate
    ```
2. Run Seeders
    ```
    npx sequelize-cli db:seed:all
    ```
3. Running :
    ```bash
    node app.js
    ```
4. or Running Using PM2 :
    ```bash
    pm2 start
    ```

View the website at: http://localhost:9000

## Supported specifications
- NodeJS
- ExpressJS
- JSON Web Token
- Postgres
- Sequelize
- Winston
- Morgan
- OpenAPI 3.x
- Swagger 2

## Documentation

View the website at: http://localhost:9000/api-docs

```
The Default Username and Password is
Username : admin
Password : admin123
```

## File Tree

![Alt text](public/file-tree.PNG?raw=true "Title")
