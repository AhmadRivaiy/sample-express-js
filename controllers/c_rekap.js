const { Op, Model, DataTypes, Sequelize, QueryTypes } = require('sequelize');
const sequelize = new Sequelize(process.env.DATABASE_CONN);
const initModels = require('../models/init-models');
var models = initModels(sequelize);

class AdminModel {
    getAllTopik = (where, limit, offset, order) => {
        return models.topik_kategori.findAndCountAll({
            include: [
                {
                    model: models.topik_visualisasi,
                    as: 'topik_visualisasi'
                }
            ],
            where: {
                ...where
            },
            order: order,
            limit: limit,
            offset: offset,
            subQuery: false
        });
    }

    getDetailTopik = (where) => {
        return models.topik_kategori.findOne({
            include: [
                {
                    model: models.topik_visualisasi,
                    as: 'topik_visualisasi'
                }
            ],
            where: {
                ...where
            }
        });
    }

    getPetaData = (where, limit, offset, order) => {
        return models.peta_jabar.findAndCountAll({
            attributes: {
                include: [
                    [Sequelize.fn('ST_AsGeoJSON', Sequelize.col('wkb_geometry')), 'geojson'],
                ],
                exclude: ['wkb_geometry']
            },
            include: [
                {
                    model: models.mst_kabupaten_kota,
                    as: 'mst_kabupaten_kotum',
                    required: true,
                    include: [
                        {
                            model: models.ppdb_penerimaan,
                            as: 'ppdb_penerimaans',
                            attributes: [where.count_key],
                            required: false,
                            separate: true,
                            where: {
                                tahun: where?.ppdb?.tahun ?? 2022
                            }
                        }
                    ]
                },
            ],
            where: {
                ...where?.wilayah
            },
            order: order,
            limit: limit,
            offset: offset,
            subQuery: false
        }).then(x => {
            let _features = [];
            x?.rows.map(y => {
                y.dataValues.geojson = {
                    "type": "Feature",
                    "properties": {
                        "name": y.dataValues?.mst_kabupaten_kotum?.kabupaten_kota,
                        "density": y.dataValues?.mst_kabupaten_kotum?.ppdb_penerimaans[0][where.count_key] ? parseFloat(y.dataValues?.mst_kabupaten_kotum?.ppdb_penerimaans[0][where.count_key]) : 0,
                    },
                    "geometry": JSON.parse(y.dataValues.geojson)
                };
                _features.push(y.dataValues.geojson);
                delete y.dataValues.geojson;
                return y;
            })
            x._features = _features;
            return x;
        })
    }
}
module.exports = AdminModel;