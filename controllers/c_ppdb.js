const { Op, Model, DataTypes, Sequelize, QueryTypes } = require('sequelize');
const sequelize = new Sequelize(process.env.DATABASE_CONN);
const initModels = require('../models/init-models');
var models = initModels(sequelize);

class AdminModel {
    getAllPPDB = (where, limit, offset, order) => {
        return models.ppdb_penerimaan.findAndCountAll({
            include: [
                {
                    model: models.mst_kabupaten_kota,
                    attributes: ['kabupaten_kota'],
                    as: 'mst_kabupaten_kotum'
                }
            ],
            where: {
                ...where
            },
            order: order,
            limit: limit,
            offset: offset,
            subQuery: false
        });
    }
}
module.exports = AdminModel;