const { Op, Model, DataTypes, Sequelize, QueryTypes } = require('sequelize');
const sequelize = new Sequelize(process.env.DATABASE_CONN);
const initModels = require('../models/init-models');
var models = initModels(sequelize);

class AdminModel {
    getAllFakta_A = (where) => {
        return models.t_fakta_a.findAll({
            where: {
                ...where
            }
        });
    }
}
module.exports = AdminModel;