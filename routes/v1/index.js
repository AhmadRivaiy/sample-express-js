//Lib
const express = require('express');
const router = express.Router();
const routerAdminPPDB = require('./admin/r_ppdb');
const routerAdminRekap = require('./admin/r_rekap');
const { isAllowedAccess } = require("../../includes/helper");

router.use('/data/ppdb', (req, res, next) => {
    const policy = {
        level: ['1', '0', 1, 0]
    }

    if(req.user_data.level && isAllowedAccess(req.user_data.level, policy)){
        next();
    }else{
        res.status(401).send({
            message: 'Anda Tidak Diperbolehkan'
        })
    }
}, routerAdminPPDB);

router.use('/data/rekap', (req, res, next) => {
    const policy = {
        level: ['1', '0', 1, 0]
    }

    if(req.user_data.level && isAllowedAccess(req.user_data.level, policy)){
        next();
    }else{
        res.status(401).send({
            message: 'Anda Tidak Diperbolehkan'
        })
    }
}, routerAdminRekap);

module.exports = router;