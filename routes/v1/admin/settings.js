const express = require('express');
const router = express.Router();
const UsersControllers = require('../../../controllers/users');
const models = new UsersControllers();
const validator = require('./authValidator');
const authChecker = require('../../../services/auth_check');
const { filterObj } = require('../../../includes/helper');
const async_utils = require('async');
const { Op, Model, QueryTypes, DataTypes, Sequelize } = require('sequelize');

//Sample Route
router.get('/', (req, res, next) => {
    res.json({
        message: 'Tes'
    })
});


/**
 * @swagger
 * /auth/register:
 *    post:
 *     description: Register User
 *     responses:
 *       200:
 *        description: Sukses
 *       422:
 *        description: Data Body Missing
 *    consumes:
 *    - application/json
 *    produces:
 *    - application/json
 *    parameters:
 *     - name: email
 *       required: true
 *       type: string
 *       in: body
 * 
*/
router.post('/register', validator.validate("check_register"), validator.verify, (req, res, next) => {
    var _data = {
        email: req.body.email,
        username: req.body.username,
        password: req.body.password,
        id_user: req.body.id_user
    }
    models.createAccountUser(_data).then(x => {
        if(x){
            res.status(201).send({
                message: 'Sukses Register User!'
            })
        }else {
            res.status(500).send({
                message: 'Gagal Register User, Email Sudah Terdaftar!'
            })
        }
    }).catch(err => {
        var details = {
            parent: err.parent,
            name: err.name,
            message: err.message
        }
        var error = new Error("Error pada server");
        error.status = 500;
        error.data = {
            date: new Date(),
            route: req.originalUrl,
            details: details
        };
        next(err);
    });
});


router.post('/forget-password', validator.validate("check_auth"), validator.verify, (req, res, next) => {
    const allowed = ['email', 'password', 'id_user'];
    const filtered = filterObj(req.body, allowed);

    async_utils.waterfall([
        function (callback) {
            models.findUsersAuth({
                email: filtered.email
            })
                .then((result) => {
                    if (result) {
                        callback(null, result);
                    } else {
                        res.status(401).send({
                            message: 'Email tidak terdaftar'
                        })
                    }
                })
        },
        function (result, callback) {
            models.updateUsersAccount({
                id_user: result.id_user,
                password: filtered.password
            }, true).then(x => {
                res.status(201).send({
                    message: 'Sukses Update Password'
                })
            }).catch(err => {
                var details = {
                    parent: err.parent,
                    name: err.name,
                    message: err.message
                }
                var error = new Error("Error pada server");
                error.status = 500;
                error.data = {
                    date: new Date(),
                    route: req.originalUrl,
                    details: details
                };
                next(err);
            })
        }
    ])
});

router.put('/update-admin', validator.validate("check_update"), validator.verify, (req, res, next) => {
    const allowed = ['email', 'old_password', 'password', 'password_confirmation'];
    const filtered = filterObj(req.body, allowed);

    filtered['id_user'] = req.user_data.id_user;
    if (filtered.password === filtered.password_confirmation) {
        async_utils.waterfall([
            function (callback) {
                models.findInfoAccountV2({
                    email: filtered.email,
                    id_user: filtered.id_user,
                    password: filtered.old_password
                })
                    .then((result) => {
                        if (result) {
                            callback(null);
                        } else {
                            res.status(403).send({
                                message: 'Akun tidak ditemukan'
                            })
                        }
                    })
            },
            function (callback) {
                var _data = {
                    password: filtered.password,
                    id_user: filtered.id_user
                }
                models.updateUsersAccount(_data, true).then(x => {
                    res.status(201).send({
                        message: 'Sukses Update Data!',
                        route: '/auth/update-user'
                    })
                }).catch(err => {
                    var details = {
                        parent: err.parent,
                        name: err.name,
                        message: err.message
                    }
                    var error = new Error("Error pada server");
                    error.status = 500;
                    error.data = {
                        date: new Date(),
                        route: req.originalUrl,
                        details: details
                    };
                    next(error);
                });
            }
        ])
    } else {
        res.status(422).send({
            message: 'Password baru tidak sama'
        })
    }
});
//End

//exports
module.exports = router;