const { query, body, validationResult, param, header, oneOf } = require('express-validator');

exports.validate = (method) => {
    switch (method) {
        case 'check_token': {
            return [
                body('token', 'token harus ada').notEmpty(),
            ]
        }
        case 'check_auth': {
            return [
                body('email', 'email harus diinput').notEmpty(),
                body('password', 'password harus diinput').notEmpty(),
            ]
        }
        case 'check_register': {
            return [
                body('email', 'email harus diinput').notEmpty(),
                body('password', 'password harus diinput').notEmpty(),
                body('id_user', 'id_user harus diinput').notEmpty(),
            ]
        }
        case 'check_update': {
            return [
                body('email', 'email -or').notEmpty(),
                body('password', 'password -or').notEmpty(),
                body('old_password', 'password_confirmation').notEmpty(),
                body('password_confirmation', 'password_confirmation').notEmpty(),
            ]
        }
        case 'create_user':{
            return [
                body('nama_user', 'Nama Tidak Boleh Kosong!').notEmpty(),
                body('email', 'Email Tidak Boleh Kosong!').notEmpty().isEmail().withMessage('Email Tidak Valid!'),
                body('password', 'Password Tidak Boleh Kosong!').notEmpty(),
                body('no_telepon', 'No. Telepon Tidak Boleh Kosong!').notEmpty().isNumeric(),
                body('tanggal_lahir', 'Tanggal Lahir Tidak Boleh Kosong!').notEmpty().isDate(),
                body('tempat_lahir', 'Tempat Lahir Tidak Boleh Kosong!').notEmpty()
            ]
        }
        case 'delete_user':{
            return [
                body('id_user', 'User Boleh Kosong!').notEmpty(),
                body('master_accounts', 'Akun Tidak Boleh Kosong!').notEmpty()
            ]
        }
        case 'update_face_user':{
            return [
                body('id_user', 'User Boleh Kosong!').notEmpty()
            ]
        }
        case 'update_user':{
            return [
                body('nama_user', 'Nama Tidak Boleh Kosong!').notEmpty(),
                body('id_jabatan', 'Jabatan Tidak Boleh Kosong!').notEmpty(),
                body('email', 'Email Tidak Boleh Kosong!').notEmpty().isEmail(),
                body('old_email', 'Email Lama Tidak Boleh Kosong!').notEmpty().isEmail(),
                body('no_telepon', 'No. Telepon Tidak Boleh Kosong!').notEmpty().isNumeric(),
                body('tanggal_lahir', 'Tanggal Lahir Tidak Boleh Kosong!').notEmpty().isDate(),
                body('tempat_lahir', 'Tempat Lahir Tidak Boleh Kosong!').notEmpty(),
            ]
        }
    }
}

exports.verify = (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.status(422).json({
                errors: errors.array()
            })
            return;
        } else {
            return next();
        }
    } catch (err) {
        return next(err);
    }
}