const express = require('express');
const router = express.Router();
const async = require('async');
const fs = require('fs');

const validator = require('./authValidator');

//Model
const ModelAdmin = require('../../../controllers/staff');
const { filterObj } = require('../../../includes/helper');
const models = new ModelAdmin();

router.get('/', (req, res, next) => {
    const dataHeader = {
        id_user: req.user_data.id_user,
        id_jabatan: req.user_data.id_jabatan
    }

    models.getAllUser(dataHeader).then(x => {
        res.status(200).send({
            status: 200,
            message: 'sukses get all staf!',
            data: x
        })
    }).catch(err => {
        var details = {
            parent: err.parent,
            name: err.name,
            message: err.message
        }
        var error = new Error("Error pada server");
        error.status = 500;
        error.data = {
            date: new Date(),
            route: req.originalUrl,
            details: details
        };
        next(error);
    });
});

router.get('/detail/:id_user', (req, res, next) => {
    const dataHeader = {
        id_user: req.user_data.id_user,
        id_jabatan: req.user_data.id_jabatan
    }
    const data = {
        id_user: req.params.id_user ?? null
    }
    models.getDetailUser(dataHeader, data).then(x => {
        res.status(200).send({
            status: 200,
            message: 'sukses get detail staf!',
            data: x
        })
    }).catch(err => {
        var details = {
            parent: err.parent,
            name: err.name,
            message: err.message
        }
        var error = new Error("Error pada server");
        error.status = 500;
        error.data = {
            date: new Date(),
            route: req.originalUrl,
            details: details
        };
        next(error);
    });
});

router.post('/create', validator.validate("create_user"), validator.verify, (req, res, next) => {
    try {
        const allowed = [
            'nama_user',
            'email',
            'password',
            'no_telepon',
            'id_jabatan',
            'tanggal_lahir',
            'tempat_lahir',
            'gelar'
        ];
        const filtered = filterObj(req.body, allowed);

        async.waterfall([
            (callback) => {
                models.checkDuplicate(filtered).then(x => {
                    if (x.length > 0) {
                        fs.unlink(path.join(__appRoot + '/upload/face_user/') + req.file.filename, function () {
                            res.status(409).send({
                                status: 409,
                                message: 'Email sudah terdaftar!'
                            })
                        });
                    } else {
                        callback(null, x);
                    }
                }).catch(err => {
                    var details = {
                        parent: err.parent,
                        name: err.name,
                        message: err.message
                    }
                    var error = new Error("Error pada server");
                    error.status = 500;
                    error.data = {
                        date: new Date(),
                        route: req.originalUrl,
                        details: details
                    };
                    next(error);
                });
            },
            (arg1, callback) => {
                models.createMasterUser(filtered).then(x => {
                    if (x) {
                        callback(null, x);
                    } else {
                        res.status(400).send({
                            status: 400,
                            message: 'Gagal membuat user!'
                        })
                    }
                }).catch(err => {
                    var details = {
                        parent: err.parent,
                        name: err.name,
                        message: err.message
                    }
                    var error = new Error("Error pada server");
                    error.status = 500;
                    error.data = {
                        date: new Date(),
                        route: req.originalUrl,
                        details: details
                    };
                    next(error);
                });
            },
            (arg1, callback) => {
                filtered['id_user'] = arg1.id_user;
                models.createAuthUser(filtered).then(x => {
                    callback(null, x);
                }).catch(err => {
                    var details = {
                        parent: err.parent,
                        name: err.name,
                        message: err.message
                    }
                    var error = new Error("Error pada server");
                    error.status = 500;
                    error.data = {
                        date: new Date(),
                        route: req.originalUrl,
                        details: details
                    };
                    next(error);
                });
            },
        ], (err, result) => {
            if (err) {
                res.status(400).send({
                    status: 400,
                    message: 'Gagal membuat user!'
                })
            } else {
                res.status(200).send({
                    status: 200,
                    message: 'Sukses membuat user!'
                })
            }
        });
    } catch (error) {
        res.status(500).send({
            message: 'Error pada Server. Hubungi admin!'
        })
    }
});

router.delete('/delete', validator.validate("delete_user"), validator.verify, (req, res, next) => {
    const allowed = [
        'id_user',
        'master_accounts',
        'id_account'
    ];
    const filtered = filterObj(req.body, allowed);

    async.waterfall([
        (callback) => {
            models.deleteMasterAccount(filtered).then(x => {
                if (x) {
                    callback(null)
                } else {
                    res.status(400).send({
                        message: 'Gagal menghapus user!'
                    })
                }
            }).catch(err => {
                var details = {
                    parent: err.parent,
                    name: err.name,
                    message: err.message
                }
                var error = new Error("Error pada server");
                error.status = 500;
                error.data = {
                    date: new Date(),
                    route: req.originalUrl,
                    details: details
                };
                next(error);
            });
        },
        (callback) => {
            models.deleteMasterUser(filtered).then(x => {
                if (x) {
                    callback(null)
                } else {
                    res.status(400).send({
                        message: 'Gagal menghapus user!'
                    })
                }
            }).catch(err => {
                var details = {
                    parent: err.parent,
                    name: err.name,
                    message: err.message
                }
                var error = new Error("Error pada server");
                error.status = 500;
                error.data = {
                    date: new Date(),
                    route: req.originalUrl,
                    details: details
                };
                next(error);
            });
        }
    ], (err, result) => {
        if (err) {
            res.status(400).send({
                status: 400,
                message: 'Gagal membuat user!'
            })
        } else {
            res.status(200).send({
                status: 200,
                message: 'Sukses menghapus user!'
            })
        }
    });
});

router.put('/update', validator.validate("update_user"), validator.verify, (req, res, next) => {
    const allowed = [
        'id_user',
        'id_jabatan',
        'nama_user',
        'no_telepon',
        'tanggal_lahir',
        'tempat_lahir',
        'gelar',
        'email',
        'old_email'
    ];
    const filtered = filterObj(req.body, allowed);

    async.waterfall([
        (callback) => {
            if (filtered.old_email != filtered.email) {
                models.checkDuplicate(filtered).then(x => {
                    if (x.length > 0) {
                        res.status(400).send({
                            message: 'Gagal! Email telah terdaftar!'
                        })
                    } else {
                        callback(null)
                    }
                }).catch(err => {
                    var details = {
                        parent: err.parent,
                        name: err.name,
                        message: err.message
                    }
                    var error = new Error("Error pada server");
                    error.status = 500;
                    error.data = {
                        date: new Date(),
                        route: req.originalUrl,
                        details: details
                    };
                    next(error);
                });
            } else {
                callback(null)
            }
        },
        (callback) => {
            models.updateUser(filtered).then(x => {
                if (x) {
                    callback(null)
                } else {
                    res.status(400).send({
                        message: 'Gagal mengubah user! Email lama tidak dikenali!'
                    })
                }
            }).catch(err => {
                var details = {
                    parent: err.parent,
                    name: err.name,
                    message: err.message
                }
                var error = new Error("Error pada server");
                error.status = 500;
                error.data = {
                    date: new Date(),
                    route: req.originalUrl,
                    details: details
                };
                next(error);
            });
        },
        (callback) => {
            models.updateAccount(filtered).then(x => {
                if (x > 0) {
                    callback(null)
                } else {
                    res.status(400).send({
                        message: 'Gagal mengubah user! Email lama tidak dikenali!'
                    })
                }
            }).catch(err => {
                var details = {
                    parent: err.parent,
                    name: err.name,
                    message: err.message
                }
                var error = new Error("Error pada server");
                error.status = 500;
                error.data = {
                    date: new Date(),
                    route: req.originalUrl,
                    details: details
                };
                next(error);
            });
        }
    ], (err, result) => {
        if (err) {
            res.status(400).send({
                status: 400,
                message: 'Gagal mengubah user!'
            })
        } else {
            res.status(200).send({
                status: 200,
                message: 'Sukses mengubah user!'
            })
        }
    });
});

module.exports = router;