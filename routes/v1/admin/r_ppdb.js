const express = require('express');
const router = express.Router();
const PPDBControllers = require('../../../controllers/c_ppdb');
const C_PPDB = new PPDBControllers();
const validator = require('./authValidator');
const authChecker = require('../../../services/auth_check');
const { filterObj, groupThreeLayerArray, getPagination, orderBySql, multipleOrderBySql } = require('../../../includes/helper');
const async_utils = require('async');
const { Op, Model, QueryTypes, DataTypes, Sequelize } = require('sequelize');

/**
 * @swagger
 * /admin/data:
 *    get:
 *     description: Get Data Table Fakta A
 *     responses:
 *       200:
 *        description: Sukses
 *       422:
 *        description: Data Body Missing
 *    consumes:
 *    - application/json
 *    produces:
 *    - application/json
 * 
*/
router.get('/', (req, res, next) => {
    // Filter
    const allowedFilter = ['tahun', 'jenjang', 'kode_kabupaten_kota'];
    const filteredFilter = filterObj(req.query, allowedFilter);

    // Pagination
    const { limit, offset } = getPagination(req.query.page, req.query.size, req.query.pagination);

    // Order by SQL
    let order = [];
    if (req.query.order_by !== undefined && req.query.order_by !== '') {
        order = multipleOrderBySql(req.query.order_by, req.query.order_type);
    }

    // Get Data
    C_PPDB.getAllPPDB(filteredFilter, limit, offset, order).then(x => {
        let _grouped = null;
        if (req.query.group_key !== undefined && req.query.group_title !== '' && req.query.groups === 'true') {
            _grouped = groupThreeLayerArray(x?.rows, req.query.group_key, req.query.group_title);
        }
        res.send({
            status: 200,
            message: 'Sukses Mengambil Data',
            grouped: _grouped,
            data: x?.rows,
            pagination: {
                page: req.query.page,
                size: req.query.size,
                count: x?.count,
            }
        })
    }).catch(err => {
        var details = {
            parent: err.parent,
            name: err.name,
            message: err.message
        }
        var error = new Error("Error pada server");
        error.status = 500;
        error.data = {
            date: new Date(),
            route: req.originalUrl,
            details: details
        };
        next(error);
    });
});
//End

//exports
module.exports = router;