const express = require('express');
const router = express.Router();

//Define Root Auth
const routerAuth = require('./v1/auth/auth');

//Define Root Settings Admin
const routerAdmin = require('./v1');

//Define Root Ref
const routerRef = require('./v1/ref');

//Define Validasi Auth 
const authChecker = require('../services/auth_check');

//Define API /auth
router.use('/v1/auth', (req, res, next) => {
    next();
}, routerAuth);

//Define API /ref
router.use('/v1/ref', (req, res, next) => {
    next();
}, routerRef);

//Define API /admin/settings
router.use('/v1/admin', authChecker.checkAuthAdmin, (req, res, next) => {
    next();
}, routerAdmin);


//Sample Route
/**
  * @swagger
  * /:
  *   get:
  *     description: Returns the homepage
  *     responses:
  *       200:
  *         description: hello world
  * definitions:
  *     Auth:
  *         type: "object"
  *         properties:
  *             token:
  *                 type: "string"
  *         xml:
  *             name: "Auth"
  */
router.get('/', (req, res, next) => {
    res.send({
        message: 'Service Dev Siterdidik',
        url: req.originalUrl
    })
});
//End Sample

//exports
module.exports = router;