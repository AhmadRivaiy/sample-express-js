var jwt = require('jsonwebtoken');
const { query, body, param, validationResult, header } = require('express-validator');
const config = require("../config/auth.config");

exports.checkAuthAdmin = (req, res, next) => {
    var headers = req.headers;
    if (typeof headers['authorization'] !== 'undefined') {
        if (headers['authorization']) {
            jwt.verify(headers['authorization'].split(";")[0].replace("Bearer ", ""), process.env.JWT_CONF_TOKEN, (err, decoded) => {
                if (!err) {
                    req.user_data = decoded;
                    return next();
                } else {
                    res.status(401).send({ message: 'Not Authorized' })
                }
            });
        } else {
            res.status(400).send({ message: 'Auth detected, but no token detected' })
        }
    } else {
        res.status(401).send({
            message: "Token Required"
        })
    }
}

exports.generateToken = (data) => {
    var tokenKey = process.env.JWT_CONF_TOKEN;
    return jwt.sign(data, tokenKey, {
        expiresIn: config.jwtExpiration
    });
    //return jwt.sign(data, process.env.JWT_CONF_TOKEN);
}