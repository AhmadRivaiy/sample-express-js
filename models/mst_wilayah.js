/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mst_wilayah', {
    kode_wilayah: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    nama: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    id_level_wilayah: {
      type: DataTypes.SMALLINT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'mst_wilayah',
    schema: 'master',
    timestamps: false
  });
};
