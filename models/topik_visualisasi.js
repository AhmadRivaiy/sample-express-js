/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('topik_visualisasi', {
    topik: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    topik_nama: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    topik_deskripsi: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'topik_visualisasi',
    schema: 'fakta',
    timestamps: false,
    indexes: [
      {
        name: "topik_pkey",
        unique: true,
        fields: [
          { name: "topik" },
        ]
      },
    ]
  });
};
