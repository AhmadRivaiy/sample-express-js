/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('peta_jabar', {
    ogc_fid: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    kabupaten: {
      type: DataTypes.STRING,
      allowNull: true
    },
    provinsi: {
      type: DataTypes.STRING,
      allowNull: true
    },
    negara: {
      type: DataTypes.STRING,
      allowNull: true
    },
    region: {
      type: DataTypes.STRING,
      allowNull: true
    },
    pulau: {
      type: DataTypes.STRING,
      allowNull: true
    },
    luas: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    kabko_2010: {
      type: DataTypes.STRING,
      allowNull: true
    },
    prov_2010: {
      type: DataTypes.STRING,
      allowNull: true
    },
    wkb_geometry: {
      type: DataTypes.BLOB,
      allowNull: true
    },
    kode_wilayah: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
  }, {
    sequelize,
    tableName: 'peta_jabar',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "peta_jabar_pkey",
        unique: true,
        fields: [
          { name: "ogc_fid" },
        ]
      },
      {
        name: "peta_jabar_wkb_geometry_geom_idx",
        fields: [
          { name: "wkb_geometry" },
        ]
      },
    ],
  });
};
