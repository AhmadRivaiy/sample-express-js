var DataTypes = require("sequelize").DataTypes;
var _log_notification = require("./log_notification");
var _master_account = require("./master_accountv2");
var _master_user = require("./master_user");
var _mst_kabupaten_kota = require("./mst_kabupaten_kota");
var _mst_wilayah = require("./mst_wilayah");
var _operator = require("./operator");
var _ppdb_penerimaan = require("./ppdb_penerimaan");
var _topik_kategori = require("./topik_kategori");
var _topik_visualisasi = require("./topik_visualisasi");
var _user_controls = require("./user_controls");
var _peta_jabar = require("./peta_jabar");

function initModels(sequelize) {
  var log_notification = _log_notification(sequelize, DataTypes);
  var master_account = _master_account(sequelize, DataTypes);
  var master_user = _master_user(sequelize, DataTypes);
  var mst_kabupaten_kota = _mst_kabupaten_kota(sequelize, DataTypes);
  var mst_wilayah = _mst_wilayah(sequelize, DataTypes);
  var operator = _operator(sequelize, DataTypes);
  var ppdb_penerimaan = _ppdb_penerimaan(sequelize, DataTypes);
  var topik_kategori = _topik_kategori(sequelize, DataTypes);
  var topik_visualisasi = _topik_visualisasi(sequelize, DataTypes);
  var user_controls = _user_controls(sequelize, DataTypes);
  var peta_jabar = _peta_jabar(sequelize, DataTypes);

  ppdb_penerimaan.belongsTo(mst_kabupaten_kota, { foreignKey: "kode_kabupaten_kota"});
  mst_kabupaten_kota.hasMany(ppdb_penerimaan, { foreignKey: "kode_kabupaten_kota"});
  topik_kategori.belongsTo(topik_visualisasi, { foreignKey: "topik_visualisasi_id"});
  topik_visualisasi.hasMany(topik_kategori, { foreignKey: "topik_visualisasi_id"});
  peta_jabar.belongsTo(mst_kabupaten_kota, { foreignKey: "kode_wilayah"});
  mst_kabupaten_kota.hasMany(peta_jabar, { foreignKey: "kode_wilayah"});
  log_notification.belongsTo(master_user, { foreignKey: "id_user"});
  master_user.hasMany(log_notification, { foreignKey: "id_user"});
  master_account.belongsTo(master_user, { foreignKey: "id_user"});
  master_user.hasMany(master_account, { foreignKey: "id_user"});
  user_controls.belongsTo(master_user, { foreignKey: "id_user"});
  master_user.hasOne(user_controls, { foreignKey: "id_user"});

  return {
    log_notification,
    master_account,
    master_user,
    mst_kabupaten_kota,
    mst_wilayah,
    operator,
    ppdb_penerimaan,
    topik_kategori,
    topik_visualisasi,
    user_controls,
    peta_jabar,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
