/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ppdb_penerimaan', {
    kode_kabupaten_kota: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      references: {
        model: 'mst_kabupaten_kota',
        key: 'kode_wilayah'
      }
    },
    ppdb_jumlah_pendaftar: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    tahun: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    ppdb_jumlah_kuota: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    ppdb_jumlah_terima: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    jenjang: {
      type: DataTypes.DOUBLE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'ppdb_penerimaan',
    schema: 'fakta',
    timestamps: false
  });
};
