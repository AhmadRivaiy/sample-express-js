/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('topik_kategori', {
    topik_kategori_id: {
      type: DataTypes.UUID,
      allowNull: false,
      defaultValue: Sequelize.fn('"user".uuid_generate_v4'),
      primaryKey: true
    },
    topik_kategori_nama: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    topik_kategori_agregasi: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    topik_kategori_nilai: {
      type: DataTypes.JSON,
      allowNull: true
    },
    topik_visualisasi_id: {
      type: DataTypes.SMALLINT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'topik_kategori',
    schema: 'fakta',
    timestamps: false,
    indexes: [
      {
        name: "topik_kategori_pkey",
        unique: true,
        fields: [
          { name: "topik_kategori_id" },
        ]
      },
    ]
  });
};
