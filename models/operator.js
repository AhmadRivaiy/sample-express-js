/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('operator', {
    uid: {
      type: DataTypes.UUID,
      allowNull: false,
      defaultValue: Sequelize.fn('"user".uuid_generate_v4'),
      primaryKey: true
    },
    npsn: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    nama_sekolah: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    password: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    create_date: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.fn('now')
    }
  }, {
    sequelize,
    tableName: 'operator',
    schema: 'user',
    timestamps: false,
    indexes: [
      {
        name: "operator_pkey",
        unique: true,
        fields: [
          { name: "uid" },
        ]
      },
    ]
  });
};
