/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('t_fakta_a', {
    id_data: {
      type: DataTypes.UUID,
      allowNull: false,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    count_a: {
      type: DataTypes.BIGINT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 't_fakta_a',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "t_fakta_a_pkey",
        unique: true,
        fields: [
          { name: "id_data" },
        ]
      },
    ]
  });
};
