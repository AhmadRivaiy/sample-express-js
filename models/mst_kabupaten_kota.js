/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mst_kabupaten_kota', {
    kode_wilayah: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      primaryKey: true
    },
    kabupaten_kota: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'mst_kabupaten_kota',
    schema: 'master',
    timestamps: false,
    indexes: [
      {
        name: "unique_wilayah",
        unique: true,
        fields: [
          { name: "kode_wilayah" },
        ]
      },
    ]
  });
};
